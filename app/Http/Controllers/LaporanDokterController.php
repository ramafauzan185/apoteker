<?php

namespace App\Http\Controllers;

use App\Models\Modeler;
use App\Models\Poli;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class LaporanDokterController extends Controller
{
    public $model;
    public function __construct()
    {
        $this->model  = new Modeler();
    }

    public function index(Request $req)
    {
        if (!isset($req->tanggal_awal)) {
            $req->tanggal_awal = Carbon::now()->format('Y-m-d');
        }

        if (!isset($req->tanggal_akhir)) {
            $req->tanggal_akhir = Carbon::now()->format('Y-m-d');
        }

        if (!isset($req->branch_id)) {
            $req->branch_id = '';
        }
        $poli = Poli::where('status', true)->orderBy('id', 'ASC')->get();
        $count['rekamMedisPasien as jumlah'] =  function ($q) use ($req) {
            $q->whereDate('created_at', '>=', $req->tanggal_awal);
            $q->whereDate('created_at', '<=', $req->tanggal_akhir);
        };

        foreach ($poli as $key => $value) {

            $count['Pendaftaran as ' . $value->id] = function ($q) use ($value, $req) {
                $q->where('poli_id', $value->id);
                $q->where('status', '!=', 'Cancel');
                $q->whereDate('tanggal', '>=', $req->tanggal_awal);
                $q->whereDate('tanggal', '<=', $req->tanggal_akhir);
            };
        }

        $data = $this->model->user()
            ->has('karyawan')
            ->where(function ($q) use ($req) {
                if (Auth::user()->akses('global')) {
                    if ($req->branch_id != '') {
                        $q->where('branch_id', $req->branch_id);
                    }
                } else {
                    $q->where('branch_id', Auth::user()->branch_id);
                }
            })
            ->whereHas('Role', function ($q) use ($req) {
                $q->where('type_role', 'DOKTER');
                // $q->where('name', '!=', 'Superuser');
            })
            ->withCount($count)
            ->get();

        return view('laporan.laporan_dokter.rekap_laporan_dokter', compact('req', 'data', 'poli'));
        // return view('laporan/laporan_pendapatan/laporan_pendapatan', compact('req', 'data', 'tanggal_awal', 'tanggal_akhir'));
    }
}
