<?php

namespace App\Http\Controllers;

use App\Models\Modeler;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LaporanJumlahPasienController extends Controller
{
    public $model;
    public function __construct()
    {
        $this->model  = new Modeler();
    }

    public function index(Request $req)
    {
        if (!isset($req->tanggal_awal)) {
            $req->tanggal_awal = dateStore();
        }

        if (!isset($req->tanggal_akhir)) {
            $req->tanggal_akhir = dateStore();
        }

        if (!isset($req->tindakan_id)) {
            $req->tindakan_id = '';
        }

        if (!isset($req->binatang_id)) {
            $req->binatang_id = '';
        }

        $branch = $this->model->branch()
            ->where(function ($q) use ($req) {
                if (Auth::user()->akses('global')) {
                    if ($req->branch_id != '') {
                        $q->where('id', $req->branch_id);
                    }
                } else {
                    $q->where('id', Auth::user()->branch_id);
                }
            })
            ->withCount(
                [
                    'rekamMedisPasien as rawatJalan' => function ($q) use ($req) {
                        $q->whereDate('mp_rekam_medis_pasien.created_at', '>=', $req->tanggal_awal);
                        $q->whereDate('mp_rekam_medis_pasien.created_at', '<=', $req->tanggal_akhir);
                        $q->where('rawat_jalan', true);
                        $q->whereHas('Pasien', function ($q) use ($req) {
                            if ($req->binatang_id != '') {
                                $q->where('binatang_id', $req->binatang_id);
                            }
                        });
                    },
                    'rekamMedisPasien as rawatInap' => function ($q) use ($req) {
                        $q->whereDate('mp_rekam_medis_pasien.created_at', '>=', $req->tanggal_awal);
                        $q->whereDate('mp_rekam_medis_pasien.created_at', '<=', $req->tanggal_akhir);
                        $q->where('rawat_inap', true);
                        $q->whereHas('Pasien', function ($q) use ($req) {
                            if ($req->binatang_id != '') {
                                $q->where('binatang_id', $req->binatang_id);
                            }
                        });
                    },
                    'rekamMedisPasien as pasienMeninggal' => function ($q) use ($req) {
                        $q->whereDate('tanggal_keluar', '>=', $req->tanggal_awal);
                        $q->whereDate('tanggal_keluar', '<=', $req->tanggal_akhir);
                        $q->whereIn('status_kepulangan', ['Pasien Meninggal']);
                        $q->whereHas('Pasien', function ($q) use ($req) {
                            if ($req->binatang_id != '') {
                                $q->where('binatang_id', $req->binatang_id);
                            }
                        });
                    },
                    'rekamMedisPasien as pasienPulang' => function ($q) use ($req) {
                        $q->whereDate('tanggal_keluar', '>=', $req->tanggal_awal);
                        $q->whereDate('tanggal_keluar', '<=', $req->tanggal_akhir);
                        $q->whereIn('status_kepulangan', ['Rekomendasi Dokter', 'Pulang Paksa']);
                        $q->whereHas('Pasien', function ($q) use ($req) {
                            if ($req->binatang_id != '') {
                                $q->where('binatang_id', $req->binatang_id);
                            }
                        });
                    },
                ],
            )
            ->get();

        $hewan = $this->model->binatang()
            ->withCount(
                [
                    'rekamMedisPasien as grooming' => function ($q) use ($req) {
                        $q->whereHas('Pendaftaran', function ($q) use ($req) {
                            if (Auth::user()->akses('global')) {
                                if ($req->branch_id != '') {
                                    $q->where('branch_id', $req->branch_id);
                                }
                            } else {
                                $q->where('branch_id', Auth::user()->branch_id);
                            }
                        });

                        $q->whereHas('RekamMedisTindakan', function ($q) use ($req) {
                            $q->whereDate('mp_rekam_medis_tindakan.created_at', '>=', $req->tanggal_awal);
                            $q->whereDate('mp_rekam_medis_tindakan.created_at', '<=', $req->tanggal_akhir);
                            $q->whereHas('Tindakan', function ($q) use ($req) {
                                $q->where('name', 'like', 'Grooming%');
                            });
                        });
                    },
                    'rekamMedisPasien as periksa' => function ($q) use ($req) {
                        $q->where(function ($q) {
                            $q->where('rawat_jalan', true);
                            $q->orWhere('rawat_inap', true);
                            $q->orWhere('titip_sehat', true);
                        });
                        $q->whereDate('mp_rekam_medis_pasien.created_at', '>=', $req->tanggal_awal);
                        $q->whereDate('mp_rekam_medis_pasien.created_at', '<=', $req->tanggal_akhir);
                        $q->whereHas('Pendaftaran', function ($q) use ($req) {
                            if (Auth::user()->akses('global')) {
                                if ($req->branch_id != '') {
                                    $q->where('branch_id', $req->branch_id);
                                }
                            } else {
                                $q->where('branch_id', Auth::user()->branch_id);
                            }
                        });
                    },
                    'rekamMedisPasien as bedah' => function ($q) use ($req) {
                        $q->whereHas('Pendaftaran', function ($q) use ($req) {
                            if (Auth::user()->akses('global')) {
                                if ($req->branch_id != '') {
                                    $q->where('branch_id', $req->branch_id);
                                }
                            } else {
                                $q->where('branch_id', Auth::user()->branch_id);
                            }
                        });

                        $q->whereHas('RekamMedisRekomendasiTindakanBedah', function ($q) use ($req) {
                            $q->whereDate('mp_rekam_medis_rekomendasi_tindakan_bedah.updated_at', '>=', $req->tanggal_awal);
                            $q->whereDate('mp_rekam_medis_rekomendasi_tindakan_bedah.updated_at', '<=', $req->tanggal_akhir);
                            $q->where('status', 'Done');
                        });
                    },
                    'rekamMedisPasien as emergency' => function ($q) use ($req) {
                        $q->whereDate('mp_rekam_medis_pasien.created_at', '>=', $req->tanggal_awal);
                        $q->whereDate('mp_rekam_medis_pasien.created_at', '<=', $req->tanggal_akhir);
                        $q->whereHas('Pendaftaran', function ($q) use ($req) {
                            if (Auth::user()->akses('global')) {
                                if ($req->branch_id != '') {
                                    $q->where('branch_id', $req->branch_id);
                                }
                            } else {
                                $q->where('branch_id', Auth::user()->branch_id);
                            }
                            $q->whereHas('Poli', function ($q) use ($req) {
                                $q->where('name', 'Emergency');
                            });
                        });
                    },
                ],
            )
            ->get();

        $tindakan = $this->model->tindakan()
            ->where(function ($q) use ($req) {
                if ($req->binatang_id != '') {
                    $q->where('binatang_id', $req->binatang_id);
                }

                if ($req->tindakan_id != '') {
                    $q->where('id', $req->tindakan_id);
                }
            })
            ->where(function ($q) use ($req) {
                $q->where(function ($q) use ($req) {
                    $q->whereHas('rekamMedisRekomendasiTindakanBedah', function ($q) use ($req) {
                        $q->whereDate('mp_rekam_medis_rekomendasi_tindakan_bedah.updated_at', '>=', $req->tanggal_awal);
                        $q->whereDate('mp_rekam_medis_rekomendasi_tindakan_bedah.updated_at', '<=', $req->tanggal_akhir);
                        $q->where('status', 'Done');
                        $q->whereHas('rekamMedisPasien.Pendaftaran', function ($q) use ($req) {
                            if (Auth::user()->akses('global')) {
                                if ($req->branch_id != '') {
                                    $q->where('branch_id', $req->branch_id);
                                }
                            } else {
                                $q->where('branch_id', Auth::user()->branch_id);
                            }
                        });
                        $q->whereHas('rekamMedisPasien', function ($q) use ($req) {
                            $q->whereHas('Pasien', function ($q) {
                                $q->whereDoesntHave('pasienMeninggal');
                            });
                        });
                    });
                });
                $q->orWhere(function ($q) use ($req) {
                    $q->whereHas('rekamMedisTindakan', function ($q) use ($req) {
                        $q->whereDate('mp_rekam_medis_tindakan.created_at', '>=', $req->tanggal_awal);
                        $q->whereDate('mp_rekam_medis_tindakan.created_at', '<=', $req->tanggal_akhir);
                        $q->whereHas('rekamMedisPasien.Pendaftaran', function ($q) use ($req) {
                            if (Auth::user()->akses('global')) {
                                if ($req->branch_id != '') {
                                    $q->where('branch_id', $req->branch_id);
                                }
                            } else {
                                $q->where('branch_id', Auth::user()->branch_id);
                            }
                        });
                        $q->whereHas('rekamMedisPasien', function ($q) use ($req) {
                            $q->whereHas('Pasien', function ($q) {
                                $q->whereDoesntHave('pasienMeninggal');
                            });
                        });
                    });
                });
            })
            ->withCount(
                [
                    'rekamMedisRekomendasiTindakanBedah as bedah' => function ($q) use ($req) {
                        $q->whereDate('mp_rekam_medis_rekomendasi_tindakan_bedah.updated_at', '>=', $req->tanggal_awal);
                        $q->whereDate('mp_rekam_medis_rekomendasi_tindakan_bedah.updated_at', '<=', $req->tanggal_akhir);
                        $q->where('status', 'Done');
                        $q->whereHas('rekamMedisPasien.Pendaftaran', function ($q) use ($req) {
                            if (Auth::user()->akses('global')) {
                                if ($req->branch_id != '') {
                                    $q->where('branch_id', $req->branch_id);
                                }
                            } else {
                                $q->where('branch_id', Auth::user()->branch_id);
                            }
                        });
                        $q->whereHas('rekamMedisPasien', function ($q) use ($req) {
                            $q->whereHas('Pasien', function ($q) {
                                $q->whereDoesntHave('pasienMeninggal');
                            });
                        });
                    },
                    'rekamMedisTindakan as tindakan' => function ($q) use ($req) {
                        $q->whereDate('mp_rekam_medis_tindakan.created_at', '>=', $req->tanggal_awal);
                        $q->whereDate('mp_rekam_medis_tindakan.created_at', '<=', $req->tanggal_akhir);
                        $q->whereHas('rekamMedisPasien.Pendaftaran', function ($q) use ($req) {
                            if (Auth::user()->akses('global')) {
                                if ($req->branch_id != '') {
                                    $q->where('branch_id', $req->branch_id);
                                }
                            } else {
                                $q->where('branch_id', Auth::user()->branch_id);
                            }
                        });
                        $q->whereHas('rekamMedisPasien', function ($q) use ($req) {
                            $q->whereHas('Pasien', function ($q) {
                                $q->whereDoesntHave('pasienMeninggal');
                            });
                        });
                    },
                ],
            )
            ->get();

        return view('management_pasien/laporan_jumlah_pasien/laporan_jumlah_pasien', compact('req', 'branch', 'hewan', 'tindakan'));
    }
}
