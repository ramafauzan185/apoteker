@extends('../layout/' . $layout)

@section('subcontent')
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.3.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/2.3.2/js/buttons.html5.min.js"></script>

    <h2 class="intro-y text-lg font-medium mt-10">Laporan Jumlah Periksa Dokter</h2>
    <div class="grid grid-cols-12 gap-6 mt-5">

        <div class="intro-y col-span-12 flex flex-wrap sm:flex-nowrap items-center justify-between mt-2">
            <div class="flex flex-wrap items-center">
            </div>

            <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">
                <div class="w-56 relative text-slate-500">
                    <input type="text" class="form-control w-56 box pr-10" id="myInputTextField" placeholder="Search...">
                    <i class="w-4 h-4 absolute my-auto inset-y-0 mr-3 right-0" data-lucide="search"></i>
                </div>
            </div>
        </div>
        <div class="col-span-12 ">
            <h5><b>Filter</b></h5>
        </div>
        @if (Auth::user()->akses('global'))
            <div class="col-span-12 md:col-span-3 mb-3">
                <label for="branch_id" class="form-label">Branch{{ dot() }}</label>
                <select name="branch_id" id="branch_id" class="select2filter form-control required">
                    <option value="">Pilih Branch</option>
                    @foreach (\App\Models\Branch::get() as $item)
                        @if (Auth::user()->akses('global'))
                            <option value="{{ $item->id }}">
                                {{ $item->kode }} - {{ $item->alamat }}</option>
                        @else
                            <option {{ Auth::user()->branch_id == $item->id ? 'selected' : '' }}
                                value="{{ $item->id }}">
                                {{ $item->kode }} - {{ $item->alamat }}</option>
                        @endif
                    @endforeach
                </select>
            </div>
        @endif
        <div class="col-span-12 md:col-span-3 mb-3">
            <label for="tanggal_awal" class="form-label">Tanggal Awal</label>
            <div class="input-group parent">
                <div class="input-group-text">
                    <i class="fas fa-calendar"></i>
                </div>
                <input id="tanggal_awal" name="tanggal_awal" type="text" class="form-control required datepicker"
                    placeholder="yyyy-mm-dd" value="{{ \carbon\carbon::parse($req->tanggal_awal)->format('Y-m-d') }}"
                    data-single-mode="true">
            </div>
        </div>
        <div class="col-span-12 md:col-span-3 mb-3">
            <label for="tanggal_akhir" class="form-label">Tanggal Akhir</label>
            <div class="input-group parent">
                <div class="input-group-text">
                    <i class="fas fa-calendar"></i>
                </div>
                <input id="tanggal_akhir" name="tanggal_akhir" type="text" class="form-control required datepicker"
                    placeholder="yyyy-mm-dd" value="{{ \carbon\carbon::parse($req->tanggal_akhir)->format('Y-m-d') }}"
                    data-single-mode="true">
            </div>
        </div>
        <div class="col-span-12 md:col-span-3">
            <label class="form-label block">&nbsp;</label>
            <button class="btn btn-primary shadow-md mr-2" onclick="filter()"><i
                    class="fas fa-search"></i>&nbsp;Search</button>

            <a href="{{ route('laporan-dokter') }}" class="btn btn-primary shadow-md mr-2">
                <i class="fas fa-refresh"></i>&nbsp;Refresh
            </a>
        </div>
        <!-- BEGIN: Data List -->
        <div class="intro-y col-span-12 p-8 mt-6 lg:mt-0 overflow-auto lg:overflow-visible rounded shadow bg-white">
            <table class="table mt-2 stripe hover table-bordered" id="table"
                style="width:100%; padding-top: 1em;  padding-bottom: 1em;">
                <thead align="center">
                    <th>No</th>
                    <th>Nama Dokter</th>
                    <th>Branch</th>
                    @foreach ($poli as $item)
                        <th>Jumlah Pasien {{ $item->name }}</th>
                    @endforeach
                    <th>Jumlah Pasien</th>
                </thead>
                <tbody>
                    @php
                        $jumlahTotal = 0;
                        $count = [];
                        foreach ($poli as $item1) {
                            $count[$item1->id] = 0;
                        }
                    @endphp
                    @foreach ($data as $i => $item)
                        @php
                            $jumlah = 0;
                        @endphp
                        <tr>
                            <td class="text-center" width=20>{{ $i + 1 }}</td>
                            <td class="text-left">{{ $item->karyawan->name }}</td>
                            <td class="text-center">{{ $item->branch->kode }}</td>
                            @foreach ($poli as $item1)
                                @php
                                    $jumlah += $item[$item1->id];
                                    $jumlahTotal += $item[$item1->id];
                                    $count[$item1->id] += $item[$item1->id];
                                @endphp
                                <td class="text-center"> {{ number_format($item[$item1->id]) }}</td>
                            @endforeach
                            <td class="text-center">{{ number_format($jumlah) }}</td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td class="text-right" colspan="3"><b><i>Total</i></b></td>
                        @foreach ($poli as $item1)
                            <td class="text-center"> {{ number_format($count[$item1->id]) }}</td>
                        @endforeach
                        <td class="text-center">
                            {{ number_format($jumlahTotal) }}
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
        <!-- END: Data List -->
    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $('.select2filter').select2({
                width: '100%',
            })
        })

        function filter(params) {
            location.href = '{{ route('laporan-dokter') }}?tanggal_awal=' + $('#tanggal_awal').val() + '&tanggal_akhir=' +
                $('#tanggal_akhir').val() + '&branch_id=' + $(
                    '#branch_id')
                .val();
        }

        // $(document).ready(function() {
        //     $('#table').DataTable({
        //         dom: 'Bfrtip',
        //         "searching": false,
        //         buttons: [
        //             'excelHtml5',
        //             'csvHtml5',
        //             'pdfHtml5'
        //         ]
        //     });
        // });
    </script>
@endsection