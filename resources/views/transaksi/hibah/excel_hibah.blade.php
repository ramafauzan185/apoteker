<table>
    <thead>
        <tr>
            <th>No</th>
            <th>No. INV</th>
            <th>Tanggal</th>
            <th>Branch</th>
            <th>Type Kasir</th>
            <th>Nama Customer</th>
            <th>Total Pembayaran</th>
            <th>Status Pembayaran</th>
            <th>Metode Pembayaran</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($data as $i => $item)
            <tr>
                <td>{{ $i + 1 }}</td>
                <td>{{ $item->kode }}</td>
                <td>{{ CarbonParse($item->tanggal, 'd-M-Y') }}</td>
                <td>{{ $item->Branch->kode }}</td>
                <td>{{ $item->type_kasir }}</td>
                <td>{{ $item->nama_owner }}</td>
                <td>{{ number_format($item->total_bayar) }}</td>
                <td>
                    @php
                        if ($item->sisa_pelunasan > 0) {
                            echo '<div class="py-1 px-2 rounded-full text-xs bg-danger text-white cursor-pointer font-medium">Belum Lunas</div>';
                        } elseif ($item->sisa_pelunasan == 0) {
                            echo '<div class="py-1 px-2 rounded-full text-xs bg-success text-white cursor-pointer font-medium">Lunas</div>';
                        }
                    @endphp
                </td>
                <td>{{ $item->metode_pembayaran }}</td> 
              
            </tr>
        @endforeach
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>

        <tr>
            <td colspan="6" class="text-center font-medium">Total Lunas</td>
            <td>{{ number_format($hibah) }}</td>
            <td></td>
            <td></td>
        </tr>

    </tbody>
</table>
