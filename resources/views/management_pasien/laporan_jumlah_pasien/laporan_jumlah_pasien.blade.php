@extends('../layout/' . $layout)
@section('content_filter')
    @include('../management_pasien/laporan_jumlah_pasien/filter_laporan_jumlah_pasien')
@endsection
@section('subcontent')
    <h2 class="intro-y text-lg font-medium mt-10">{{ convertSlug($global['title']) }}</h2>
    <div class="grid grid-cols-12 gap-6 mt-5">

        <div class="intro-y col-span-12 flex flex-wrap sm:flex-nowrap items-center justify-between mt-2">
            <div class="flex flex-wrap items-center">
                <div class="dropdown inline">
                    <button class="dropdown-toggle btn px-2 box" aria-expanded="false" data-tw-toggle="dropdown">
                        <span class="w-5 h-5 flex items-center justify-center">
                            <i class="w-4 h-4" data-lucide="plus"></i>
                        </span>
                    </button>
                    <div class="dropdown-menu w-40 ">
                        <ul class="dropdown-content">
                            <li>
                                <a href="javascript:;" class="dropdown-item" onclick="openFilter()">
                                    <i class="w-4 h-4 mr-2 fa-solid fa-filter"></i>
                                    Filter
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">
                <div class="w-56 relative text-slate-500">
                    <input type="text" class="form-control w-56 box pr-10" id="myInputTextField" placeholder="Search...">
                    <i class="w-4 h-4 absolute my-auto inset-y-0 mr-3 right-0" data-lucide="search"></i>
                </div>
            </div>
        </div>
        <div class="col-span-12 ">
            <h5><b>Filter</b></h5>
        </div>
        <!-- BEGIN: Data List -->
        <div class="intro-y col-span-12 p-8 mt-6 lg:mt-0 overflow-auto lg:overflow-visible rounded shadow bg-white"
            id="isi">
            <div class="flex justify-between">
                <h5 class="font-bold text-2xl">Laporan Jumlah Pasien Berdasarkan Pasien Masuk/Keluar</h5>
                <div class="col-span-12 text-right mb-3">
                    <button class="btn btn-primary shadow-md mr-2" onclick="excel()">Export Excel</button>
                </div>
            </div>
            <div class="flex">
                {{ CarbonParseISO($req->tanggal_awal, 'DD-MMMM-Y') }} s/d
                {{ CarbonParseISO($req->tanggal_akhir, 'DD-MMMM-Y') }}
            </div>
            <br>
            <table class="table mt-2 stripe hover table-bordered" id="table"
                style="width:100%; padding-top: 1em;  padding-bottom: 1em;">
                <thead align="center">
                    <th>Branch</th>
                    <th>Rawat Jalan</th>
                    <th>Rawat Inap</th>
                    <th>Pasien Meninggal</th>
                    <th>Pasien Pulang</th>
                    {{-- <th>Total</th> --}}
                </thead>
                <tbody>
                    @php
                        $rawatJalan = 0;
                        $rawatInap = 0;
                        $pasienMeninggal = 0;
                        $pasienPulang = 0;
                    @endphp
                    @foreach ($branch as $item)
                        @php
                            $rawatJalan += $item->rawatJalan;
                            $rawatInap += $item->rawatInap;
                            $pasienMeninggal += $item->pasienMeninggal;
                            $pasienPulang += $item->pasienPulang;
                        @endphp
                        <tr>
                            <td>{{ $item->kode }} - {{ $item->alamat }}</td>
                            <td class="text-right">{{ number_format($item->rawatJalan) }}</td>
                            <td class="text-right">{{ number_format($item->rawatInap) }}</td>
                            <td class="text-right">{{ number_format($item->pasienMeninggal) }}</td>
                            <td class="text-right">{{ number_format($item->pasienPulang) }}</td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td class="text-right"><b><i>Total</i></b></td>
                        <td class="text-right">{{ number_format($rawatJalan) }}</td>
                        <td class="text-right">{{ number_format($rawatInap) }}</td>
                        <td class="text-right">{{ number_format($pasienMeninggal) }}</td>
                        <td class="text-right">{{ number_format($pasienPulang) }}</td>
                    </tr>
                </tfoot>
            </table>
        </div>
        <!-- END: Data List -->
        <div class="intro-y col-span-12 p-8 mt-6 lg:mt-0 overflow-auto lg:overflow-visible rounded shadow bg-white"
            id="isi1">
            <div class="flex justify-between">
                <h5 class="font-bold text-2xl">Laporan Jumlah Pasien Berdasarkan Poli</h5>
                <div class="col-span-12 text-right  mb-3">
                    <button class="btn btn-primary shadow-md mr-2" onclick="excel1()">Export Excel</button>
                </div>

            </div>
            <div class="flex">
                {{ CarbonParseISO($req->tanggal_awal, 'DD-MMMM-Y') }} s/d
                {{ CarbonParseISO($req->tanggal_akhir, 'DD-MMMM-Y') }}
            </div>
            <br>
            <table class="table mt-2 stripe hover table-bordered" id="table"
                style="width:100%; padding-top: 1em;  padding-bottom: 1em;">
                <th>Hewan</th>
                <th>Grooming</th>
                <th>Periksa</th>
                <th>Bedah</th>
                <th>Emergency</th>
                <th>Total</th>
                </thead>
                <tbody>
                    @php
                        $grooming = 0;
                        $periksa = 0;
                        $bedah = 0;
                        $emergency = 0;
                    @endphp
                    @foreach ($hewan as $item)
                        @php
                            $grooming += $item->grooming;
                            $periksa += $item->periksa;
                            $bedah += $item->bedah;
                            $emergency += $item->emergency;
                        @endphp
                        <tr>
                            <td>{{ $item->name }}</td>
                            <td class="text-center">{{ number_format($item->grooming) }}</td>
                            <td class="text-center">{{ number_format($item->periksa) }}</td>
                            <td class="text-center">{{ number_format($item->bedah) }}</td>
                            <td class="text-center">{{ number_format($item->emergency) }}</td>
                            <td class="text-center">
                                {{ number_format($item->grooming + $item->periksa + $item->bedah + $item->emergency) }}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td class="text-right"><b><i>Total</i></b></td>
                        <td class="text-center">{{ number_format($grooming) }}</td>
                        <td class="text-center">{{ number_format($periksa) }}</td>
                        <td class="text-center">{{ number_format($bedah) }}</td>
                        <td class="text-center">{{ number_format($emergency) }}</td>
                        <td class="text-center">
                            {{ number_format($grooming + $periksa + $bedah + $emergency) }}
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
        <div class="intro-y col-span-12 p-8 mt-6 lg:mt-0 overflow-auto lg:overflow-visible rounded shadow bg-white"
            id="isi2">
            <div class="flex justify-between">
                <h5 class="font-bold text-2xl">Laporan Jumlah Pasien Berdasarkan Tindakan</h5>
                <div class="col-span-12 text-right  mb-3">
                    <button class="btn btn-primary shadow-md mr-2" onclick="excel2()">Export Excel</button>
                </div>
            </div>
            <div class="flex">
                {{ CarbonParseISO($req->tanggal_awal, 'DD-MMMM-Y') }} s/d
                {{ CarbonParseISO($req->tanggal_akhir, 'DD-MMMM-Y') }}
            </div>
            <br>
            <table class="table mt-2 stripe hover table-bordered" id="table"
                style="width:100%; padding-top: 1em;  padding-bottom: 1em;">
                <thead align="center">
                    <th>Tindakan</th>
                    <th>Hewan</th>
                    <th>Total</th>
                </thead>
                <tbody>
                    @foreach ($tindakan as $item)
                        <tr>
                            <td>{{ $item->name }}</td>
                            <td>{{ $item->binatang ? $item->binatang->name : '-' }}</td>
                            <td class="text-center">{{ number_format($item->bedah + $item->tindakan) }}</td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td class="text-right" colspan="2"><b><i>Total</i></b></td>
                        <td class="text-center">{{ number_format($tindakan->sum('bedah') + $tindakan->sum('tindakan')) }}
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <div id="xlsDownload" style="display: none"></div>
@endsection
@section('script')
    <script>
        $(document).ready(function() {
            $('.select2filter').select2({
                dropdownParent: $("#slide-over-filter .modal-body"),
            })
        })

        function filter(params) {
            @if (Auth::user()->akses('global'))
                location.href = '{{ route('laporan-jumlah-pasien') }}?tanggal_awal=' + $('#tanggal_awal').val() +
                    '&tanggal_akhir=' + $('#tanggal_akhir').val() + '&branch_id=' + $(
                        '#branch_id').val() + '&binatang_id=' + $('#binatang_id').val() + '&tindakan_id=' + $(
                        '#tindakan_id').val();
            @else
                location.href = '{{ route('laporan-jumlah-pasien') }}?tanggal_awal=' + $('#tanggal_awal').val() +
                    '&tanggal_akhir=' + $('#tanggal_akhir').val() + '&binatang_id=' + $('#binatang_id').val() +
                    '&tindakan_id=' + $('#tindakan_id').val();
            @endif
        }

        function openFilter() {
            slideOver.toggle();
        }

        function excel(argument) {
            var blob = b64toBlob(btoa($('div[id=isi]').html().replace(/[\u00A0-\u2666]/g, function(c) {
                return '&#' + c.charCodeAt(0) + ';';
            })), "application/vnd.ms-excel");
            var blobUrl = URL.createObjectURL(blob);
            var dd = new Date()
            var ss = '' + dd.getFullYear() + "-" +
                (dd.getMonth() + 1) + "-" +
                (dd.getDate()) +
                "_" +
                dd.getHours() +
                dd.getMinutes() +
                dd.getSeconds()

            $("#xlsDownload").html("<a href=\"" + blobUrl + "\" download=\"Download_Laporan_Amore\_" + ss +
                "\.xls\" id=\"xlsFile\">Download</a>");
            $("#xlsFile").get(0).click();

            function b64toBlob(b64Data, contentType, sliceSize) {
                contentType = contentType || '';
                sliceSize = sliceSize || 512;

                var byteCharacters = atob(b64Data);
                var byteArrays = [];


                for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                    var slice = byteCharacters.slice(offset, offset + sliceSize);

                    var byteNumbers = new Array(slice.length);
                    for (var i = 0; i < slice.length; i++) {
                        byteNumbers[i] = slice.charCodeAt(i);
                    }

                    var byteArray = new Uint8Array(byteNumbers);

                    byteArrays.push(byteArray);
                }

                var blob = new Blob(byteArrays, {
                    type: contentType
                });
                return blob;
            }
        }

        function excel1(argument) {
            var blob = b64toBlob(btoa($('div[id=isi1]').html().replace(/[\u00A0-\u2666]/g, function(c) {
                return '&#' + c.charCodeAt(0) + ';';
            })), "application/vnd.ms-excel");
            var blobUrl = URL.createObjectURL(blob);
            var dd = new Date()
            var ss = '' + dd.getFullYear() + "-" +
                (dd.getMonth() + 1) + "-" +
                (dd.getDate()) +
                "_" +
                dd.getHours() +
                dd.getMinutes() +
                dd.getSeconds()

            $("#xlsDownload").html("<a href=\"" + blobUrl + "\" download=\"Download_Laporan_Amore\_" + ss +
                "\.xls\" id=\"xlsFile\">Download</a>");
            $("#xlsFile").get(0).click();

            function b64toBlob(b64Data, contentType, sliceSize) {
                contentType = contentType || '';
                sliceSize = sliceSize || 512;

                var byteCharacters = atob(b64Data);
                var byteArrays = [];


                for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                    var slice = byteCharacters.slice(offset, offset + sliceSize);

                    var byteNumbers = new Array(slice.length);
                    for (var i = 0; i < slice.length; i++) {
                        byteNumbers[i] = slice.charCodeAt(i);
                    }

                    var byteArray = new Uint8Array(byteNumbers);

                    byteArrays.push(byteArray);
                }

                var blob = new Blob(byteArrays, {
                    type: contentType
                });
                return blob;
            }
        }

        function excel2(argument) {
            var blob = b64toBlob(btoa($('div[id=isi2]').html().replace(/[\u00A0-\u2666]/g, function(c) {
                return '&#' + c.charCodeAt(0) + ';';
            })), "application/vnd.ms-excel");
            var blobUrl = URL.createObjectURL(blob);
            var dd = new Date()
            var ss = '' + dd.getFullYear() + "-" +
                (dd.getMonth() + 1) + "-" +
                (dd.getDate()) +
                "_" +
                dd.getHours() +
                dd.getMinutes() +
                dd.getSeconds()

            $("#xlsDownload").html("<a href=\"" + blobUrl + "\" download=\"Download_Laporan_Amore\_" + ss +
                "\.xls\" id=\"xlsFile\">Download</a>");
            $("#xlsFile").get(0).click();

            function b64toBlob(b64Data, contentType, sliceSize) {
                contentType = contentType || '';
                sliceSize = sliceSize || 512;

                var byteCharacters = atob(b64Data);
                var byteArrays = [];


                for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                    var slice = byteCharacters.slice(offset, offset + sliceSize);

                    var byteNumbers = new Array(slice.length);
                    for (var i = 0; i < slice.length; i++) {
                        byteNumbers[i] = slice.charCodeAt(i);
                    }

                    var byteArray = new Uint8Array(byteNumbers);

                    byteArrays.push(byteArray);
                }

                var blob = new Blob(byteArrays, {
                    type: contentType
                });
                return blob;
            }
        }
    </script>
@endsection
